<?php

/**
 * IsAdminQuestion
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class IsAdminQuestion extends PluginBase
{
    protected static $description = 'Set Y value on a question if currentuser have LimeSurvey administration right on current survey.';
    protected static $name = 'IsAdminQuestion';

    /** @var integer|null keep track of surveyId **/
    private $surveyId;

    /** @inheritdoc **/
    public function init()
    {
        $this->subscribe('beforeQuestionRender', 'hideAnswerPart');
        $this->subscribe('newQuestionAttributes', 'addIsAdminAttributes');
        /* in beforeSurveyPage : best way */
        $this->subscribe('beforeSurveyPage', 'beforeSurveyPage');
        /* In expression manager */
        $this->subscribe('setVariableExpressionEnd');
    }

    /* Add the uniqueId attribute in registered attributes */
    public function addIsAdminAttributes()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $attributeEvent = $this->getEvent();
        $IsAdminAttributes = array(
            'useAsIsAdmin' => array(
                'name' => 'useAsIsAdmin',
                'types'     => 'S', /* Only for short text */
                'category'  => $this->translate('Is admin'),
                'sortorder' => 200,
                'inputtype' => 'switch',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes')
                ),
                'caption' => $this->translate('Use this question to check admin right.'),
                'help' => $this->translate('If current participant doing survey have responses update admin right : set this questioin value to Y, empty string if not.'),
                'default' => 0,
            ),
        );
        $attributeEvent->append('questionAttributes', $IsAdminAttributes);
    }

    /* Hide the answer part according to attribute settings */
    public function hideAnswerPart()
    {
        $beforeQuestionEvent = $this->getEvent();
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($beforeQuestionEvent->get('qid'));
        if (!empty($aAttributes['useAsIsAdmin'])) {
            $sid = $beforeQuestionEvent->get('surveyId');
            $questionColumn = $sid . 'X' . $beforeQuestionEvent->get('gid') . 'X' . $beforeQuestionEvent->get('qid');
            /* No need to check else there are an issue … */
            $beforeQuestionEvent->set('answers', CHtml::hiddenField(
                $questionColumn,
                $_SESSION['survey_' . $sid][$questionColumn],
                array('id' => 'answer' . $questionColumn)
            ));
        }
    }

    /* Set admin id un case */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('surveyId');
        if (empty($_SESSION['survey_' . $surveyId]['insertarray'])) {
            /* Last chance for all in one survey of for question in 1st group */
            /* Happen for all page before page is shown, don't broke on version lesser than 3 since event didn't exist */
            $this->surveyId = $surveyId;
            $this->subscribe('getPluginTwigPath', 'setAdminId');
            $this->subscribe('beforeQuestionRender', 'beforeQuestionRender');
            return;
        }
        if (!isset($_SESSION['survey_' . $surveyId]['IsAdminQuestions'])) {
            $this->surveyId = $surveyId;
            $this->setAdminId();
        }
    }

    /* setAndFixUniqId */
    public function beforeQuestionRender()
    {
        $surveyId = $this->surveyId;
        if (!isset($_SESSION['survey_' . $surveyId]['IsAdminQuestions'])) {
            $this->setAdminId();
        }
        $this->unsubscribe('beforeQuestionRender');
        $this->hideAnswerPart();
        $this->subscribe('beforeQuestionRender', 'hideAnswerPart');
    }

    /* Just do it after expression manager constructed */
    public function setVariableExpressionEnd() {
        $surveyId = $this->getEvent()->get('surveyId');
        if (!isset($_SESSION['survey_' . $surveyId]['IsAdminQuestions'])) {
            $this->surveyId = $surveyId;
            $this->setAdminId();
        }
    }

    /**
     *
    /* Set the unique id for all needed question for current survey */
    public function setAdminId()
    {
        $surveyId = $this->surveyId;
        $this->unsubscribe('getPluginTwigPath');
        if (!isset($_SESSION['survey_' . $surveyId]['insertarray'])) {
            return;
        }
        if (isset($_SESSION['survey_' . $surveyId]['IsAdminQuestions'])) {
            return;
        }
        if (intval(Yii::app()->getConfig('versionnumber')) > 3) {
            $aQuestionsIsAdmin = Question::model()->with('questionattributes')->findAll(
                'questionattributes.attribute = :attribute AND questionattributes.value = :value and sid =:sid',
                array(
                    ':attribute' => 'useAsIsAdmin',
                    ':value' => 1,
                    ':sid' => $surveyId
                )
            );
        } else {
            $criteria = new CDbCriteria();
            $criteria->join = 'LEFT JOIN {{question_attributes}} as question_attributes ON question_attributes.qid=t.qid';
            $criteria->condition = 't.sid = :sid and attribute = :attribute AND value = :value';
            $criteria->params = array(':sid' => $surveyId, ':attribute' => 'useAsIsAdmin', ':value' => 1);
            $aQuestionsIsAdmin = Question::model()->findAll($criteria);
        }
        $srid = isset($_SESSION['survey_' . $surveyId]['srid']) ? $_SESSION['survey_' . $surveyId]['srid'] : null;
        $oResponse = null;
        if ($srid && Survey::model()->findByPk($surveyId)->active == "Y") {
            $oResponse = Response::model($surveyId)->findByPk($srid);
        }
        $IsAdminQuestions = array();
        $IsAdminValue = "";
        $havePermssion = Permission::model()->hasSurveyPermission($surveyId, 'responses', 'update');
        if ($havePermssion) {
            $IsAdminValue = "Y";
        }
        if (!empty($aQuestionsIsAdmin)) {
            foreach ($aQuestionsIsAdmin as $aQuestionIsAdmin) {
                $questionColumn = $aQuestionIsAdmin->sid . 'X' . $aQuestionIsAdmin->gid . 'X' . $aQuestionIsAdmin->qid;
                $IsAdminQuestions[] = $questionColumn;
                $_SESSION['survey_' . $surveyId][$questionColumn] = $IsAdminValue;
                LimeExpressionManager::SetVariableValue("=", $questionColumn, $IsAdminValue);
                if ($oResponse) {
                    $oResponse->$questionColumn = $_SESSION['survey_' . $surveyId][$questionColumn];
                }
            }
            if ($oResponse) {
                $oResponse->save();
            }
        }
        $_SESSION['survey_' . $surveyId]['IsAdminQuestions'] = $IsAdminQuestions;
    }

    /**
    * Alternative of 3.X parent::gT using unescaped by default
    */
    private function translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (is_callable($this, 'gT')) {
            return $this->gT($sToTranslate, $sEscapeMode, $sLanguage);
        }
        return $sToTranslate;
    }
}
