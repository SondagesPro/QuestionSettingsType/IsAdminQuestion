# IsAdminQuestion

A plugin for LimeSurvey to check if current particpant is adon

## Installation

See [Install and activate a plugin for LimeSurvey](https://extensions.sondages.pro/about/install-and-activate-a-plugin-for-limesurvey.html).

## Usage

- Activate the plugin
- Create a short text question type
- In _Input_ set _Use this question to check admin right.._ to Yes
- Use this question like all other Expression Manager variables.

_tip_ You can use hidden attribute to totally hide this question to participant.

_tip_ In 5.X version, it's bettre to use [FunctionAdminRight](https://gitlab.com/SondagesPro/ExpressionManager/FunctionAdminRight).


### Sample usage

In a survey with token and token anser persistnce enabled : an administrator can review reponse by user and have some specific question.

Survey can be used with reloadAnyResponse plugin.

## Contribute

Issue and pull request are welcome on [gitlab](https://gitlab.com/SondagesPro/QuestionSettingsType/IsAdminQuestion).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2023 Denis Chenu <https://sondages.pro>
- [Support](https://support.sondages.pro)
- [Donate](https://support.sondages.pro/open.php?topicId=12)
- [Donate on Liberapay](https://liberapay.com/SondagesPro/)
- [Donate on Open Collective](https://opencollective.com/sondagespro)
Distributed under [GNU AFFERO GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/agpl.txt) licence
